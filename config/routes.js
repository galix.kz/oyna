/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'pages/homepage'
  },
  'POST /jwt': 'AuthController.jwt',
  'POST /team/upload': 'TeamController.uploadAvatar',
  '/team/avatar/:id': 'TeamController.downloadAvatar',
  'POST /team/find_team': 'TeamController.findTeam',
  'POST /gameuser/upload': 'GameuserController.uploadAvatar',
  '/gameuser/avatar/:id': 'GameuserController.downloadAvatar',
  'POST /change_password_with_jwt': 'AuthController.change_password_with_jwt',
  'POST /confirm_code': 'AuthController.confirm_code',
  'POST /change_password': 'AuthController.change_password',
  'POST /generate_code': 'AuthController.generate_code',

  'POST /gamehistory/wait_for_confirmation': 'GameHistoryController.wait_for_confirmation',
  'POST /gamehistory/confirm/:id': 'GameHistoryController.confirm',
  'POST /gamehistory/accept_invitation': 'GameHistoryController.accept_invitation',
  'POST /gamehistory/game_invitation': 'GameHistoryController.game_invitation',
  'POST /gamehistory/remove_invitation': 'GameHistoryController.remove_invitation',
  'GET /gamehistory/find_games/:id': 'GameHistoryController.find_games',
  'GET /gameuser/me': 'AuthController.me',

  'POST /teammember/apply': 'TeamMemberController.apply',
  'POST /teammember/user_invitation': 'TeamMemberController.user_invitation',
  'POST /teammember/remove_user_invitation': 'TeamMemberController.remove_user_invitation',
  'POST /teammember/cancel_invitation': 'TeamMemberController.cancel_invitation',
  'POST /teammember/remove_member': 'TeamMemberController.remove_member_by_user_id',
  'POST /teammember/remove_application': 'TeamMemberController.remove_application',
  'POST /teammember/cancel_application': 'TeamMemberController.cancel_application',
  '/gameuser/subscribeToNotifications/:id': 'GameuserController.subscribeToNotifications',
  'POST /field/find_nearest': 'FieldController.findNearestField',
  'POST /field/upload': 'FieldController.uploadAvatar',
  'POST /field/update_rating': 'FieldController.update_rating',
  '/field/avatar/*': 'FieldController.downloadAvatar',
  'GET /socket_test': {
    view: 'pages/socket_test'
  }
  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝



  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝


};
