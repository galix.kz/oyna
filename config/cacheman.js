module.exports.cacheman = {
  driver: 'redis',


  redis: {
    port: 6379,
    host: 'localhost',
    engine: 'cacheman-redis',
  },

}
