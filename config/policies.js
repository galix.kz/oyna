/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/
  GameuserController: {
    // find: 'isAuthenticated',
    findone: 'isAuthenticated',
    // uploadAvatar: 'isAuthenticated',
    // downloadAvatar: 'isAuthenticated',
  },
  TeamController:{
    // find: 'isAuthenticated',
    // findone: 'isAuthenticated',
    create: 'isAuthenticated',
    update: 'isAuthenticated',
    upload: 'isAuthenticated',
    download: 'isAuthenticated',
  },
  TeamMemberController:{
    find: 'isAuthenticated',
    findone: 'isAuthenticated',
    create: 'isAuthenticated',
    update: 'isAuthenticated',
  },
  GameHistoryController:{
    // find: 'isAuthenticated',
    // findone: 'isAuthenticated',
    // create: 'isAuthenticated',
    // update: 'isAuthenticated',
  },
  MadeGoalsController:{
    find: 'isAuthenticated',
    findone: 'isAuthenticated',
    create: 'isAuthenticated',
    update: 'isAuthenticated',
  },
  AuthController: {
    change_password_with_jwt: 'isAuthenticated'
  }
  // '*': true,

};
