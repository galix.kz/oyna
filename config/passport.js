const passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  bcrypt = require('bcrypt-nodejs');
const passportJWT = require("passport-jwt");
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});
passport.deserializeUser(function(id, cb){
  User.findOne({id}, function(err, user) {
    cb(err, users);
  });
});
passport.use(new LocalStrategy({
  usernameField: 'phone_number',
  passportField: 'password'
}, function(username, password, cb){
  (async function () {
      let user = await Gameuser.findOne({phone_number: username}).populate('TeamCaptain');
      if(!user) return cb(null, false, {message: 'Username not found'});
      let team_member = await TeamMember.find({member:user.id}).populate('team');
      let team_list = []
      team_member.forEach(function (team) {
        if(team['team']!==null) team_list.push(team['team'])
      })
      user['team'] = team_list
      console.log(team_member['team'])
      console.log('________________________________________-')

      bcrypt.compare(password, user.password, function(err, res){
        if(!res) return cb(null, false, { message: 'Invalid Password' });
        return cb(null, user, {message: 'Logged In Successfully'});
      });
  })()

}));

passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey   : 'your_jwt_secret'
  },
  function (jwtPayload, cb) {
    //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
    return (async function () {
      let user = await Gameuser.findOne({id: jwtPayload.id}).populate('TeamCaptain');
      if(!user) return cb('User not found', null)
      let team_member = await TeamMember.find({member: jwtPayload.id}).populate('team');
      // console.log(team_member)
      let team_list = []
      team_member.forEach(function (team) {
        if(team['team']!==null) team_list.push(team['team'])
      })

      user['team'] = team_list
      // console.log(team_member)
      if (user) return cb(null, user);


    })();
  }
));
