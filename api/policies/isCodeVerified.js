const cache = require('sailsjs-cacheman').sailsCacheman('redis');

module.exports = async function (req, res, proceed) {
  if (!req.body.phone_number) return res.forbidden();
  cache.get('code_verified_'+req.body.phone_number, function (cache_err, verified) {
    if (cache_err || verified==null || verified) return res.status(400).send('Code not verified. ' +
      'Please generate new code and verify it');
    return proceed()
  });

};
