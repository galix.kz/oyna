const passport = require('passport');
const jwt = require('jsonwebtoken');

module.exports = async function (req, res, proceed) {
  passport.authenticate('jwt', {session: false},(err,user) => {
    if(user) return proceed();
    return res.status(400).json({'message':'Forbidden'});
  })(req,res);

};
