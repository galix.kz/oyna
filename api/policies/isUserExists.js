const cache = require('sailsjs-cacheman').sailsCacheman('redis');

module.exports = async function (req, res, proceed) {
  const phone_number = req.body.phone_number;
  if (!phone_number) return res.status(400).send('Please send phone_number');

  const user = await Gameuser.findOne({phone_number: phone_number});
  console.log(user)
  if (!user) return res.status(404).send('There is no user with this phone_number')
  return proceed()
};
