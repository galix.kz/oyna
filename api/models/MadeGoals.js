/**
 * MadeGoals.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    user:{
      model: 'Gameuser'
    },
    count: {
      type: 'number',
      required: true,
    },
    game_history: {
      model: 'GameHistory'
    }
  },

};

