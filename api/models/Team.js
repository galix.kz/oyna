/**
 * Team.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name:{
      type:'string',
      required: true,
      unique: true,
    },
    avatarUrl:{
      type: 'string',
      allowNull: true,
    },
    avatarFd: {
      type: 'string',
      allowNull: true,
    },
    games_count:{
      type: 'number',
      defaultsTo: 0,
      allowNull: true,
    },
    wins_count: {
      type: 'number',
      defaultsTo: 0,
      allowNull: true,
    },
    captain:{
      model: 'Gameuser',
    },
    members:{
      collection: 'TeamMember',
      via: 'team',
    },

  },
  afterCreate: function(team, next) {
    (async function(){
    await TeamMember.create({team:team.id,member: team.captain})
    next();
    })();
  },
  beforeDestroy: async function(team,cb){
    await TeamMember.destroy({team: _.pluck(team, 'id')});
    await GameHistory.destroy({team1: _.pluck(team, 'id')});
    await GameHistory.destroy({team2: _.pluck(team, 'id')});
    return cb();
  }
};

