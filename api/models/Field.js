/**
 * Field.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true,
    },
    phone_number: {
      type: 'string',
      required: true,
    },
    address: {
      type: 'string',
      required: true,
    },
    longitude: {
      type: 'string',
      required: true,
    },
    latitude: {
      type: 'string',
      required: true,
    },
    work_time: {
      type: 'json',
      required: true,
      description: 'Send here working time as you want',
    },
    price: {
      type: 'number',
      required: true,
    },
    type_of_field:{
      type: 'number',
      isIn: [1,2],
      extendedDescription: '1 - not artificial, 2 - artificial',
      required: true,
    },
    city:{
      type: 'String',
      required: true,
    },
    rating: {
      type: 'number',
      defaultsTo: 0
    },
    rating_sum:{
      type: 'number',
      defaultsTo: 0,
    },
    voted_amount: {
      type: 'number',
      defaultsTo: 0
    },
    stadion:{
      type: 'number',
      isIn: [1,2],
      extendedDescription: '1 - open, 2 closed',
      required: true,
    },
    avatarUrl:{
      type: 'json',
      // columnType: 'array'
    },
  },
  afterCreate: function(field, cb){
    field.avatarUrl = [];
    return cb();
  },
};

