/**
 * Gameuser.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const bcrypt = require('bcrypt-nodejs');

var request = require('request');

module.exports = {
  attributes: {
    phone_number:{
      type: 'string',
      required: true,
      unique: true,
      custom: function(value){
        // Validation rule
        return _.isString(value) && value.length >= 10 && value.match(/[0-9]/);
      }
    },
    gender:{
      type: 'string',
      isIn: ['male','female'],
      required: true,
    },
    birth_date:{
      type: 'string',
      required: true,
    },
    first_name: {
      type: 'string',
      required: true,
    },
    last_name: {
      type: 'string',
      required: true,
    },
    password: {
      type: 'string',
      required: true
    },
    city: {
      type: 'string',
      required: true,
    },
    avatarUrl:{
      type: 'string',
      allowNull: true,
    },
    avatarFd: {
      type: 'string',
      allowNull: true,
    },
    TeamCaptain: {
      collection: 'Team',
      via: 'captain',
    },
    team:{
      collection: 'TeamMember',
      via: 'member'
    },
    goals_count: {
      type: 'number',
      defaultsTo: 0,
    }
  },
  customToJSON: function() {
    return _.omit(this, ['password'])
  },
  beforeCreate: function(user, cb){
    bcrypt.genSalt(10, function(err, salt){
      bcrypt.hash(user.password, salt, null, function(err, hash){
        if(err) return cb(err);
        user.password = hash;
        return cb();
      });
    });
  },
  afterCreate(user,cb){
    const baseUrl = sails.config.custom.baseUrl;
    return cb()
  },
  beforeDestroy: async function(user,cb){
    await TeamMember.destroy({member: _.pluck(user, 'id')});
    await Team.destroy({captain: _.pluck(user,'id')})
    await MadeGoals.destroy({user: _.pluck(user,'id')})
    return cb();
  }
};
