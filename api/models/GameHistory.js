/**
 * GameHistory.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    team1:{
      model: 'Team',
    },
    team2:{
      model: 'Team',
    },
    score_team1:{
      type: 'number',
      allowNull: true,
      defaultsTo: 0,
    },
    score_team2:{
      type: 'number',
      allowNull: true,
      defaultsTo: 0,
    },
    field:{
      model: 'Field',
    },
    date: {
      type: 'ref',
      required: true,
    },
    confirmed: {
      type: 'boolean',
      defaultsTo: false,
    },
    canceled: {
      type: 'boolean',
      defaultsTo: false,
    },
    madeGoals:{
      collection: 'MadeGoals',
      via: 'game_history'
    }
  },

};

