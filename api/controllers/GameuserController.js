/**
 * GameuserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const passport = require('passport');
const cache = require('sailsjs-cacheman').sailsCacheman('redis');
module.exports = {


  uploadAvatar: function (req, res) {

    req.file('avatar').upload({
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 10000000
    }, function whenDone(err, uploadedFiles) {
  if (err) {
    return res.serverError(err);
  }
  let user_id = null;

  // If no files were uploaded, respond with an error.
  if (uploadedFiles.length === 0) return res.status(400).json({"message": "error"})
    // Get the base URL for our deployed application from our custom config
    // (e.g. this might be "http://foobar.example.com:1339" or "https://example.com")
    const baseUrl = sails.config.custom.baseUrl;
    passport.authenticate('jwt', {session: false},(err,user) => {
      user_id = user.id
      console.log(user.id)

      // Save the "fd" and the url where the avatar for a gameuser can be accessed
      Gameuser.update(user_id, {

        // Generate a unique URL where the avatar can be downloaded.
        avatarUrl: require('util').format('%s/gameuser/avatar/%s', baseUrl, user_id),

        // Grab the first file and use it's `fd` (file descriptor)
        avatarFd: uploadedFiles[0].fd
      }).fetch()
        .exec(function (err,game_user){
          if (err) return res.status(400).json({"message":err});
          console.log(game_user);
          return res.json({"message": game_user});
        });
    })(req,res);
  });
},
  subscribeToNotifications: function(req, res) {
    console.log('')
    // if (!req.isSocket) {
    //   return res.status(400).json({"message": "You should connect with socket"});
    // }

    var roomName = 'user_notification_'+req.param('id');
    (async function() {
      let notifications = await cache.get(roomName);
      console.log(notifications)
      if (!notifications) {
        notifications = {}
        cache.set(roomName, notifications, '30d')
      }
      // cache.del(roomName)
      sails.sockets.join(req, roomName, function (err) {
        if (err) {
          return res.serverError(err);
        }

        return res.json({
          message: 'Subscribed to user ' + req.param('id') + ' notifications',
          notifications: notifications
        });
      });
    })();
  },
  downloadAvatar: function (req, res){
    Gameuser.findOne(req.param('id')).exec( function (err, gameuser){
      if (err) return res.status(400).json({"message": err});
      if (!gameuser) return res.status(404).json({"message":"notFound"});

      // gameuser has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!gameuser.avatarFd) {
        return res.status(404).json({"message":"notFound"});
      }
      console.log(gameuser)
      const SkipperDisk = require('skipper-disk');
      const fileAdapter = SkipperDisk({});

      fileAdapter.read(gameuser.avatarFd)
        .on('error', function (err){
          return res.status(400).json({"message": err});
        })
        .pipe(res);
    });
  }
};

