/**
 * GameHistoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const cache = require('sailsjs-cacheman').sailsCacheman('redis');
module.exports = {
  game_invitation: function(req,res){
    const team1_id = req.body.team1;
    const team2_id = req.body.team2;
    const date = req.body.date;
    const field = req.body.field;
    if(!team1_id || !team2_id || !date) return res.status(400).json({"message":"please check all fields"});
    (async function() {
      const team2 = await Team.findOne(team2_id);
      const team1 = await Team.findOne(team1_id);
      if(!team1 || !team2) return res.status(404).json({"message":"team1 or team2 not found"});
      let notification1 = await cache.get('user_notification_'+team1.captain);
      let notification2 = await cache.get('user_notification_'+team2.captain);
      let game_invitations_income = notification2.game_invitations_income;
      let game_invitations_outcome = notification1.game_invitations_outcome;
      if(!game_invitations_income){
        game_invitations_income={}
      }
      if(!game_invitations_outcome){
        game_invitations_outcome={}
      }
      let data_to_save = {
        team1: team1,
        team2: team2,
        date: date,
        field: field,
      }
      game_invitations_outcome['team_'+team2.id] = data_to_save;
      game_invitations_income['team_'+team1.id] = data_to_save;
      console.log(game_invitations_income)
      console.log(game_invitations_outcome)
      notification1.game_invitations_outcome = game_invitations_outcome;
      notification2.game_invitations_income = game_invitations_income;
      cache.set('user_notification_'+team2.captain, notification2,'30d');
      cache.set('user_notification_'+team1.captain, notification1,'30d');
      sails.sockets.broadcast('user_notification_'+team2.captain,'notification',
        { type: 'game_invitation',
          message:  data_to_save
        });
      return res.json(data_to_save)
    })();
  },
  remove_invitation: function(req,res){
    const team1_id = req.body.team1;
    const team2_id = req.body.team2;
    if(!team1_id || !team2_id) return res.status(400).json({"message":"please check all fields"});
    (async function () {
      const team1 = await Team.findOne(team1_id);
      const team2 = await Team.findOne(team2_id);
      // console.log(team2.captain)
      if(!team1 || !team2) return res.status(404).json({"message":"team1 or team2 not found"});
      let notification1 = await cache.get('user_notification_'+team1.captain);
      let notification2 = await cache.get('user_notification_'+team2.captain);
      const game_invitations_income = JSON.parse(JSON.stringify(notification2.game_invitations_income));
      const game_invitations_outcome = JSON.parse(JSON.stringify(notification1.game_invitations_outcome));
      sails.sockets.broadcast(['user_notification_'+team2.captain,'user_notification_'+team1.captain],'notification',
        { type: 'game_invitation_removed',
          message:  game_invitations_outcome['team_'+team2.id]
        });

      delete notification2.game_invitations_income['team_'+team1.id];
      delete notification1.game_invitations_outcome['team_'+team2.id];


      cache.set('user_notification_'+team2.captain, notification2,'30d');
      cache.set('user_notification_'+team1.captain, notification1,'30d');

      res.json(game_invitations_outcome['team_'+team2.id])
    })()
  },
  accept_invitation: function(req,res){
    const params = req.body;
    const accept = req.body.accept;
    delete req.body.accept;
    if(!accept || !req.body.team1) return res.status(400).json({"message":"please check all fields"});
    (async function () {
      const team1 = await Team.findOne(req.body.team1);
      const team2 = await Team.findOne(req.body.team2);
      let notification = await cache.get('user_notification_'+team1.captain);
      let game_invitations_result = notification.game_invitations_result;
      if(!game_invitations_result) game_invitations_result = [];
      let data_to_save;
      if(accept==="false"){
        data_to_save = {
          "accept": "false",
          "detail": params,
        }
      }
      else{
        const game_history = await GameHistory
          .create(params)
          .fetch().intercept((err) => {
            return res.status(400).json({"message": err})
          });
        data_to_save = {
          "accept": "true",
          "detail": game_history,
        }

      }
      data_to_save['team1'] = team1;
      data_to_save['team2'] = team2;
      game_invitations_result.push(data_to_save)
      notification.game_invitations_result = game_invitations_result;
      cache.set('user_notification_'+team1.captain,notification,'30d')
      sails.sockets.broadcast('user_notification_'+team1.captain,'notification',
        { type: 'game_invitations',
          message:  data_to_save});
      return res.json(data_to_save)
    })()
  },
  wait_for_confirmation: function(req,res){
    const game_history_id = req.body.game_history_id;
    const score1 = req.body.score_team1;
    const score2 = req.body.score_team2;
    if(!score1 || !score2 || !game_history_id){
      return res.status(400).json({"message":"please check that all fields"})
    }
    (async function () {
      let game_history = await GameHistory.findOne(game_history_id).populate('team1').populate('team2');
      // console.log(game_history)
      if(!game_history) return res.status(404).json({"message": "Game History did not found"});
      let team2_captain_id = game_history.team2.captain;
      let notification = await cache.get('user_notification_'+team2_captain_id)
      let data_to_save = {game_history: game_history,score_team1:score1,score_team2:score2}
      let waiting_for_confirmation = notification['waiting_for_confirmation']
      if(!waiting_for_confirmation){
        waiting_for_confirmation = []
      }
      waiting_for_confirmation.push(data_to_save);
      notification.waiting_for_confirmation = waiting_for_confirmation;
      cache.set('user_notification_'+team2_captain_id,notification,'30d')
      cache.set('waiting_for_confirmation_'+game_history.id,data_to_save,'30d')
      sails.sockets.broadcast('user_notification_'+team2_captain_id,'notification',

        { type: 'waiting_for_confirmation',
          message:  data_to_save});
      res.json(game_history)
    })();
  },
  find_games: function(req,res){
    const team_id = req.param('id');
    if(!team_id) return res.status(400).json({"message":"Please make sure you set all fields"});
    (async function () {
      game_history = await sails.getDatastore().sendNativeQuery("SELECT * FROM gamehistory WHERE team1="+team_id+" OR team2="+team_id);
      res.json(game_history.rows)
    })()
  },
  confirm: function (req,res) {
    const game_history_id = req.param('id');
    const confirm = req.body.confirm;
    // console.log('confirm')
    (async ()=>{

      const waiting_for_confirmation = await cache.get('waiting_for_confirmation_'+game_history_id);
      const team1_captain_id = waiting_for_confirmation.game_history.team1.captain;
      // const team1_captain_id = 1;
      let notification = await cache.get('user_notification_'+ team1_captain_id)
      let game_history_confirm = notification.game_history_confirm;
      let data_to_save;
      if (!game_history_confirm) {
        game_history_confirm = []
      }
      if(!confirm) return res.status(400).
                              json({"message": 'please enter confirm value is correct'});
      if(!waiting_for_confirmation) return res.status(404).
                                                json({"message": 'please make sure that game history id is correct'});
      console.log(waiting_for_confirmation);
      if(confirm==='false') {
        data_to_save = {confirm: "false", game_history: waiting_for_confirmation.game_history}
        game_history_confirm.push(data_to_save);
      }
      else {
        data_to_save = {confirm: "true", game_history: waiting_for_confirmation.game_history}
        game_history_confirm.push(data_to_save);
        await GameHistory.update({ id : waiting_for_confirmation.game_history.id}).set
        ({score_team1:waiting_for_confirmation.score_team1, score_team2: waiting_for_confirmation.score_team2}).fetch().intercept((err) => {
          return res.status(400).json({"message":err})
        });
        const team2 = waiting_for_confirmation.game_history.team2;
        const team1 = waiting_for_confirmation.game_history.team1;
        const score1 = waiting_for_confirmation.score_team1;
        const score2 = waiting_for_confirmation.score_team2;
        if(score1>score2) {
          await Team.update(team1.id).set({wins_count:team1.wins_count+1, games_count:team1.games_count+1}).fetch();
          await Team.update(team2.id).set({games_count:team2.games_count+1}).fetch();
        }
        if(score1<score2) {
          await Team.update(team2.id).set({wins_count:team2.wins_count+1, games_count:team2.games_count+1}).fetch();
          await Team.update(team1.id).set({games_count: team1.games_count + 1}).fetch();
        }
        if(score1===score2){
          await Team.update(team1.id).set({games_count:team1.games_count+1}).fetch();
          await Team.update(team2.id).set({games_count:team2.games_count+1}).fetch();
        }
      }
      // return res.json(game_history2)
      notification.game_history_confirm = game_history_confirm;
      cache.set('user_notification_'+ team1_captain_id,notification,'30d')
      sails.sockets.broadcast('user_notification_' + team1_captain_id, 'notification',
        {
          type: 'game_history_confirm',
          message: data_to_save
        });
      return res.json(waiting_for_confirmation)
    })();
  }
};

