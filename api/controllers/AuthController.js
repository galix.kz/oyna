/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const cache = require('sailsjs-cacheman').sailsCacheman('redis');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');

function generate(){
  let code = '';
  for(let i=0;i<7;i++){
    code += Math.floor(Math.random()*10);
  }
  return code;
}

module.exports = {
  jwt: function(req, res) {
    console.log(req.body)
    passport.authenticate('local', {session: false}, (err, user, info) => {
      console.log(user);
      if (err || !user) {
        return res.status(400).json({
          message: 'Something is not right',
          user   : user
        });
      }
      req.login(user, {session: false}, (err) => {
        if (err) {
          res.send(err);
        }
        // generate a signed son web token with the contents of user object and return it in the response
        const token = jwt.sign(user.toJSON(), 'your_jwt_secret',{expiresIn: '365d' });
        // const token = 'qwe';
        return res.json({user, token});
      });
    })(req, res);
  },
  me: function(req,res){
    passport.authenticate('jwt', {session: false},(err,user) => {
      console.log(user)
      if(user) return res.json(user.toJSON());
      return res.status(400).json({'message':'Forbidden'});
    })(req,res);
  },
  change_password_with_jwt: function(req,res){
    let previous_password = req.body.previous_password;
    let new_password = req.body.new_password;
    if (!previous_password || !new_password) return res.status(400).json({"message": "Please enter all fields" });
    passport.authenticate('jwt', {session: false},(err,user) => {
      bcrypt.compare(previous_password, user.password, function(err, result){
        if(!result) return res.status(403).json({"message": 'Invalid Previous Password' });
        bcrypt.genSalt(10, function(err, salt){
          bcrypt.hash(new_password, salt, null, function(err, hash){
            if(err) return cb(err);
            new_password = hash;
            (async function(){
              let updated_user = await Gameuser.update(user.id).set({password:new_password}).fetch()
              return res.json(updated_user)
            })();
          });
        });
      });
      // if(user) return res.json(user.toJSON());
      // return res.status(400).json({'message':'Forbidden'});
    })(req,res);
  },
  generate_code: function(req,res){
    const cache = require('sailsjs-cacheman').sailsCacheman('redis');
    const request = require('request');
    const phone_number = req.body.phone_number;
    if (!phone_number) return res.status(400).json({"message":"Please set phone_number"}.toJSON());
    let code = generate();
    cache.set('generate_code_'+code,phone_number,'1h');
    // request.get({
    //   url: "https://smsc.kz/sys/send.php?login=game.account12&psw=GameAccount123!&name=oy-na&phones="
    //   +phone_number+"&mes=Verification-"+code,
    // }, function(error, response, body) {
    //   if (error) {
    //     sails.log.error(error);
    //   }
    //   else{
    //     console.log(response)
    //   }
    // });
    return res.json({"code":code});
  },
  confirm_code: function(req,res){
    let code = req.body.code;
    cache.get('generate_code_'+code, function (cache_err, phone_number) {
      if (cache_err || !phone_number) return res.status(400).json({'message':'Wrong code'});
      try{
        return res.json({'message':"ok"});
      }
      catch (model_err) {
        throw model_err;
      }
    });
  },
  change_password: function(req,res){
    let new_password = req.body.password;
    let code = req.body.code;
    if (!code) return res.status(400).json({'message':'Please enter code'});
    if (!new_password) return res.status(400).json({'message':'Please enter password'});
    bcrypt.genSalt(10, function(err, salt){
      bcrypt.hash(new_password, salt, null, function(err, hash){
        if(err) return res.status(500).json({'message': err});
        new_password = hash;
      });
    });
    cache.get('generate_code_'+code, function (cache_err, phone_number) {
      if (cache_err || !phone_number) return res.status(400).json({'message':'Wrong code'});
      try{
        async function a() {
          return await Gameuser.update({phone_number:phone_number}).set({password:new_password}).fetch();
        }
        a().then(result=>{
          return res.json({"message": result})
        }).catch(err => {
          return res.status(400).json({"message": err})
        });

      }
      catch (model_err) {
        throw model_err;
      }
    });

  },


};
