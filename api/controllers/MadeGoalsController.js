/**
 * MadeGoalsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  create: function(req, res) {
    const params = req.body;
    (async function () {
      made_goals = await MadeGoals
        .createEach(params)
        .fetch().intercept((err) => {
          return res.status(400).json({"message": err})
        });
      for(let i=0;i<params.length;i++){
        let update = await sails.getDatastore().sendNativeQuery("UPDATE gameuser SET goals_count = goals_count + "+params[i]['count'] +
          " WHERE id="+params[i]['user']);
        console.log(update)

      }

      return res.json(made_goals)
    })()
  },
  find: function (req,res) {
    var parseBlueprintOptions = req.options.parseBlueprintOptions || req._sails.config.blueprints.parseBlueprintOptions;

    // Set the blueprint action for parseBlueprintOptions.
    req.options.blueprintAction = 'find';

    var queryOptions = parseBlueprintOptions(req);
    // var TeamMember = req._sails.Model[queryOptions.using];
    (async function () {
      const query = await MadeGoals.find(queryOptions.criteria, queryOptions.populates).meta(queryOptions.meta)
        .intercept((err) => {
          if (err) {
            // If this is a usage error coming back from Waterline,
            // (e.g. a bad criteria), then respond w/ a 400 status code.
            // Otherwise, it's something unexpected, so use 500.
            switch (err.name) {
              case 'UsageError': return res.badRequest(formatUsageError(err, req));
              default: return res.serverError(err);
            }
          }//-•

        })
      for(let i=0;i<query.length;i++) {
        game_history = await GameHistory.findOne(query[i].game_history.id).populate('team1').populate('team2');
        query[i].game_history = game_history
      }
      return res.send(query)
    })();


  }
};

