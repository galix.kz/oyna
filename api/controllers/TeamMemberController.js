/**
 * TeamMemberController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const cache = require('sailsjs-cacheman').sailsCacheman('redis');
module.exports = {
  find: function (req, res) {

  var parseBlueprintOptions = req.options.parseBlueprintOptions || req._sails.config.blueprints.parseBlueprintOptions;

  // Set the blueprint action for parseBlueprintOptions.
  req.options.blueprintAction = 'find';

  var queryOptions = parseBlueprintOptions(req);
  // var TeamMember = req._sails.Model[queryOptions.using];
     (async function () {
       matchingRecords = await TeamMember
         .find(queryOptions.criteria, queryOptions.populates).meta(queryOptions.meta);

          for(let i=0;i<matchingRecords.length;i++){
            let count=0;
            if( matchingRecords[i].team === null){

            }
            else {
              count = await TeamMember.count({team: matchingRecords[i].team['id']});
            }
            matchingRecords[i]['team_member_count'] = count
          }

           return res.ok(matchingRecords);

     })();
  },
  apply: function(req,res){
    const team_id = req.body.team_id;
    const user_id = req.body.user_id;
    if (!team_id || !user_id) return res.status(400).send('Please make sure that you sent ' +
      'user_id or team_id');
    (async function () {
      let team = await Team.findOne(team_id);
      let user = await Gameuser.findOne(user_id);
      if (!team) return res.status(404).json({'message':'Team does not exists'});
      if (!user) return res.status(404).json({'message':'User does not exists'});
      let captain_notification = await cache.get('user_notification_'+team.captain);
      let user_notification = await cache.get('user_notification_'+user.id);
      let applied_users_list = captain_notification.applied_users_list;
      let applied_team_list = user_notification.applied_team_list;
      if(!applied_team_list) applied_team_list = [];
      if (!applied_users_list){
        applied_users_list = []
      }
      applied_team_list.push(team);
      applied_users_list.push(user)
      captain_notification.applied_users_list = applied_users_list;
      user_notification.applied_team_list = applied_team_list;
      cache.set('user_notification_'+team.captain,captain_notification,'30d');
      cache.set('user_notification_'+user.id,user_notification,'30d');
      sails.sockets.broadcast('user_notification_'+team.captain,'notification',
        { type: 'applied_user',
          message:  user});
      res.json(team)
    })()
  },
  remove_application: async function(req,res){
    const team_id = req.body.team_id;
    const user_id = req.body.user_id;
    if (!team_id || !user_id) return res.status(400).send('Please make sure that you sent ' +
      'user_id or team_id');
      let team = await Team.findOne(team_id);
      let user = await Gameuser.findOne(user_id);
      if (!team) return res.status(404).json({'message':'Team does not exists'});
      if (!user) return res.status(404).json({'message':'User does not exists'});

      let captain_notification = await cache.get('user_notification_'+team.captain);
      let user_notification = await cache.get('user_notification_'+user.id);

      let applied_users_list = captain_notification.applied_users_list;
      let applied_team_list = user_notification.applied_team_list;

      let to_delete_team_array_id = applied_team_list.findIndex(x => x.id === team.id);
      let to_delete_user_array_id = applied_users_list.findIndex(x => x.id === user.id)

      applied_users_list.splice(to_delete_user_array_id,1);
      applied_team_list.splice(to_delete_team_array_id,1);

      captain_notification.applied_users_list = applied_users_list;
      user_notification.applied_team_list = applied_team_list;
      cache.set('user_notification_'+team.captain,captain_notification,'30d');
      cache.set('user_notification_'+user.id,user_notification,'30d');
      sails.sockets.broadcast('user_notification_'+team.captain,'notification',
        { type: 'remove_application',
          message:  user});
      return res.json(team)
  },
  cancel_application: async function(req,res){
    const team_id = req.body.team_id;
    const user_id = req.body.user_id;
    if (!team_id || !user_id) return res.status(400).send('Please make sure that you sent ' +
      'user_id or team_id');
    let team = await Team.findOne(team_id);
    let user = await Gameuser.findOne(user_id);
    if (!team) return res.status(404).json({'message':'Team does not exists'});
    if (!user) return res.status(404).json({'message':'User does not exists'});

    let captain_notification = await cache.get('user_notification_'+team.captain);
    let user_notification = await cache.get('user_notification_'+user.id);

    let applied_users_list = captain_notification.applied_users_list;
    let applied_team_list = user_notification.applied_team_list;
    let canceled_team_list = user_notification.canceled_team_list;

    let to_delete_team_array_id = applied_team_list.findIndex(x => x.id === team.id);
    let to_delete_user_array_id = applied_users_list.findIndex(x => x.id === user.id)

    if(!canceled_team_list) canceled_team_list = [];

    applied_users_list.splice(to_delete_user_array_id,1);
    applied_team_list.splice(to_delete_team_array_id,1);
    canceled_team_list.push(team)

    captain_notification.applied_users_list = applied_users_list;
    user_notification.applied_team_list = applied_team_list;
    user_notification.canceled_team_list = canceled_team_list;
    cache.set('user_notification_'+team.captain,captain_notification,'30d');
    cache.set('user_notification_'+user.id,user_notification,'30d');
    sails.sockets.broadcast('user_notification_'+team.captain,'notification',
      { type: 'cancel_application',
        message:  user});
    return res.json(team)
  },
  remove_member_by_user_id: async function(req,res){
    const team_id = req.body.team_id;
    const user_id = req.body.user_id;
    if (!team_id || !user_id) return res.status(400).json({
      "message": 'Please make sure that you sent ' +
        'user_id or team_id'
    });
    let team_member = await TeamMember.destroy({team: team_id, member: user_id}).fetch();
    if (!team_member) return res.status(404).json({
      "message": 'Could not find team member please check user_id and team_id'
    });
    const team = await Team.findOne(team_id);
    const user = await Gameuser.findOne(user_id);

    sails.sockets.broadcast('user_notification_'+team.captain,'notification',
      { type: 'remove_team_member',
        message:  user});
    sails.sockets.broadcast('user_notification_'+user.id,'notification',
      { type: 'remove_team_member',
        message:  team});
    return res.json(team_member);
  },
  user_invitation: function (req,res) {
    const team_id = req.body.team_id;
    const user_id = req.body.user_id;
    if (!team_id || !user_id) return res.status(400).json('Please make sure that you sent ' +
      'user_id or team_id');
    (async function () {
      let team = await Team.findOne(team_id);
      let user = await Gameuser.findOne(user_id);
      if (!team) return res.status(404).json({'message':'Team does not exists'});
      if (!user) return res.status(404).json({'message':'User does not exists'});
      let captain_notification = await cache.get('user_notification_'+team.captain);
      let user_notification = await cache.get('user_notification_'+user.id);

      let invited_users_list = captain_notification.invited_users_list;
      let invited_teams_list = user_notification.invited_teams_list;
      let canceled_team_list = user_notification.canceled_team_list;
      if(!invited_users_list) invited_users_list = [];
      if(!invited_teams_list) invited_teams_list = [];
      invited_teams_list.push(team);
      invited_users_list.push(user);

      captain_notification.invited_users_list = invited_users_list;
      user_notification.invited_teams_list = invited_teams_list;
      console.log(invited_users_list)
      cache.set('user_notification_'+team.captain,captain_notification,'30d');
      cache.set('user_notification_'+user.id,user_notification,'30d');

      sails.sockets.broadcast('user_notification_'+user.id,'notification',
        { type: 'invitation_to_team',
          message:  team});
      res.json(user)
    })();
  },
  remove_user_invitation: function (req,res) {
    const team_id = req.body.team_id;
    const user_id = req.body.user_id
    if (!team_id || !user_id) return res.status(400).send('Please make sure that you sent ' +
      'user_id or team_id');
    (async function () {
      let team = await Team.findOne(team_id);
      let user = await Gameuser.findOne(user_id);
      if (!team) return res.status(404).json({'message':'Team does not exists'});
      if (!user) return res.status(404).json({'message':'User does not exists'});
      let captain_notification = await cache.get('user_notification_'+team.captain);
      let user_notification = await cache.get('user_notification_'+user.id);

      let invited_users_list = captain_notification.invited_users_list;
      let invited_teams_list = user_notification.invited_teams_list;

      if(!invited_users_list) invited_users_list = [];
      if(!invited_teams_list) invited_teams_list = [];

      let to_delete_team_array_id = invited_teams_list.findIndex(x => x.id === team.id);
      let to_delete_user_array_id = invited_users_list.findIndex(x => x.id === user.id);

      invited_users_list.splice(to_delete_user_array_id,1);
      invited_teams_list.splice(to_delete_team_array_id,1);

      captain_notification.invited_users_list = invited_users_list;
      user_notification.invited_teams_list = invited_teams_list;

      cache.set('user_notification_'+team.captain,captain_notification,'30d');
      cache.set('user_notification_'+user.id,user_notification,'30d');

      sails.sockets.broadcast('user_notification_'+user.id,'notification',
        { type: 'remove_invitation_to_team',
          message:  team});
      res.json(user)
    })();
  },
  cancel_invitation: async function(req,res){
    const team_id = req.body.team_id;
    const user_id = req.body.user_id;
    if (!team_id || !user_id) return res.status(400).send('Please make sure that you sent ' +
      'user_id or team_id');
    let team = await Team.findOne(team_id);
    let user = await Gameuser.findOne(user_id);
    if (!team) return res.status(404).json({'message':'Team does not exists'});
    if (!user) return res.status(404).json({'message':'User does not exists'});
    let captain_notification = await cache.get('user_notification_'+team.captain);
    let user_notification = await cache.get('user_notification_'+user.id);

    let invited_users_list = captain_notification.invited_users_list;
    let invited_teams_list = user_notification.invited_teams_list;
    let canceled_users_list = user_notification.canceled_users_list;
    if(!invited_users_list) invited_users_list = [];
    if(!invited_teams_list) invited_teams_list = [];

    let to_delete_team_array_id = invited_teams_list.findIndex(x => x.id === team.id);
    let to_delete_user_array_id = invited_users_list.findIndex(x => x.id === user.id);

    invited_users_list.splice(to_delete_user_array_id,1);
    invited_teams_list.splice(to_delete_team_array_id,1);
    if(!canceled_users_list) canceled_users_list = [];
    canceled_users_list.push(team)

    captain_notification.invited_users_list = invited_users_list;
    user_notification.invited_teams_list = invited_teams_list;
    user_notification.canceled_users_list = canceled_users_list;
    cache.set('user_notification_'+team.captain,captain_notification,'30d');
    cache.set('user_notification_'+user.id,user_notification,'30d');

    sails.sockets.broadcast('user_notification_'+user.id,'notification',
      { type: 'cancel_invitation',
        message:  team});
    res.json(user)
  },
};

