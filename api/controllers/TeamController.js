/**
 * TeamController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var util = require('util');
var _ = require('@sailshq/lodash');
var formatUsageError = require(__dirname + '/../../node_modules/sails/lib/hooks/blueprints/formatUsageError.js');

module.exports = {

  findOne: function(req, res) {

    var parseBlueprintOptions = req.options.parseBlueprintOptions || req._sails.config.blueprints.parseBlueprintOptions;

    // Set the blueprint action for parseBlueprintOptions.
    req.options.blueprintAction = 'findOne';

    var queryOptions = parseBlueprintOptions(req);

    console.log(__dirname)
    // Only use the `where`, `select` or `omit` from the criteria (nothing else is valid for findOne).
    queryOptions.criteria = _.pick(queryOptions.criteria, ['where', 'select', 'omit']);

    // Only use the primary key in the `where` clause.
    queryOptions.criteria.where = _.pick(queryOptions.criteria.where, Team.primaryKey);


    (async function () {
     matchingRecord = await Team
      .findOne(queryOptions.criteria, queryOptions.populates).meta(queryOptions.meta)

        // if (err) {
        //   // If this is a usage error coming back from Waterline,
        //   // (e.g. a bad criteria), then respond w/ a 400 status code.
        //   // Otherwise, it's something unexpected, so use 500.
        //   switch (err.name) {
        //     case 'UsageError':
        //       return res.badRequest(formatUsageError(err, req));
        //     default:
        //       return res.serverError(err);
        //   }
        // }//-•
        console.log('_________________________________________________________________________')
        // let new_team_member_list = []

          team_member = await TeamMember.find({team:matchingRecord.id}).populate('member')
          console.log(team_member)
        matchingRecord['members'] = team_member
        //
        //   console.log(matchingRecord.id)





        // matchingRecord['members'] = new_team_member_list;
        if (!matchingRecord) {
          req._sails.log.verbose('In `findOne` blueprint action: No record found with the specified id (`' + queryOptions.criteria.where[Team.primaryKey] + '`).');
          return res.notFound();
        }

        if (req._sails.hooks.pubsub && req.isSocket) {
          Team.subscribe(req, [matchingRecord[Team.primaryKey]]);
          actionUtil.subscribeDeep(req, matchingRecord);
        }

        return res.ok(matchingRecord);


    })();
  },
  uploadAvatar: function (req, res) {
    const team_id = req.body.team_id;
    if(!team_id) return res.status(400).json({"message":"Please make sure that you entered team_id"})
    console.log(req.body)
    req.file('avatar').upload({
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 10000000
    },function whenDone(err, uploadedFiles) {
      if (err) {
        return res.serverError(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0){
        return res.badRequest('No file was uploaded');
      }

      // Get the base URL for our deployed application from our custom config
      // (e.g. this might be "http://foobar.example.com:1339" or "https://example.com")
      const baseUrl = sails.config.custom.baseUrl;
      console.log();
      // Save the "fd" and the url where the avatar for a team can be accessed
      Team.update(team_id, {

        // Generate a unique URL where the avatar can be downloaded.
        avatarUrl: require('util').format('%s/team/avatar/%s', baseUrl, req.body.team_id),

        // Grab the first file and use it's `fd` (file descriptor)
        avatarFd: uploadedFiles[0].fd
      })
        .exec(function (err){
          if (err) return res.serverError(err);
          return res.ok();
        });
    });
  },
  downloadAvatar: function (req, res){
    Team.findOne(req.param('id')).exec( function (err, team){
      if (err) return res.serverError(err);
      if (!team) return res.notFound();

      // team has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!team.avatarFd) {
        return res.notFound();
      }

      const SkipperDisk = require('skipper-disk');
      const fileAdapter = SkipperDisk({});

      fileAdapter.read(team.avatarFd)
        .on('error', function (err){
          return res.serverError(err);
        })
        .pipe(res);
    });
  },
  findTeam: function (req,res) {
    find_team = req.body.find_team;
    (async function(){
      console.log(find_team)
      team = await sails.getDatastore().sendNativeQuery("SELECT * FROM team WHERE name LIKE '%"+find_team+"%'",);
      res.json({"teams":team["rows"]})
    })();

  },
  update: async function(req,res){

    var parseBlueprintOptions = req.options.parseBlueprintOptions || req._sails.config.blueprints.parseBlueprintOptions;

    // Set the blueprint action for parseBlueprintOptions.
    req.options.blueprintAction = 'update';

    var queryOptions = parseBlueprintOptions(req);
    var Team = req._sails.models[queryOptions.using];

    var criteria = {};
    criteria[Team.primaryKey] = queryOptions.criteria.where[Team.primaryKey];

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // FUTURE: Use a database transaction here, if supported by the datastore.
    // e.g.
    // ```
    // Team.getDatastore().transaction(function during(db, proceed){ ... })
    // .exec(function afterwards(err, result){}));
    // ```
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // Find and update the targeted record.
    //
    // (Note: this could be achieved in a single query, but a separate `findOne`
    //  is used first to provide a better experience for front-end developers
    //  integrating with the blueprint API.)
    var query = Team.findOne(_.cloneDeep(criteria), _.cloneDeep(queryOptions.populates));
    query.exec(function found(err, matchingRecord) {
      if (err) {
        switch (err.name) {
          case 'UsageError': return res.badRequest(formatUsageError(err, req));
          default: return res.serverError(err);
        }
      }//-•

      if (!matchingRecord) { return res.notFound(); }

      Team.update(_.cloneDeep(criteria), queryOptions.valuesToSet).meta(queryOptions.meta).exec(async function updated(err, records) {

        // Differentiate between waterline-originated validation errors
        // and serious underlying issues. Respond with badRequest if a
        // validation error is encountered, w/ validation info, or if a
        // uniqueness constraint is violated.
        if (err) {
          switch (err.name) {
            case 'AdapterError':
              switch (err.code) {
                case 'E_UNIQUE': return res.badRequest(err);
                default: return res.serverError(err);
              }//•
            case 'UsageError': return res.badRequest(formatUsageError(err, req));
            default: return res.serverError(err);
          }
        }//-•

        // If we didn't fetch the updated instance, just return 'OK'.
        if (!records) {
          return res.ok();
        }

        if (!_.isArray(records)) {
          return res.serverError('Consistency violation: When `fetch: true` is used, the second argument of the callback from update should always be an array-- but for some reason, it was not!  This should never happen... it could be due to a bug or partially implemented feature in the database adapter, or some other unexpected circumstance.');
        }

        // Because this should only update a single record and update
        // returns an array, just use the first item.  If more than one
        // record was returned, something is amiss.
        if (!records.length || records.length > 1) {
          req._sails.log.warn(
            util.format('Unexpected output from `%s.update`.', Team.globalId)
          );
        }

        var updatedRecord = records[0];

        var pk = updatedRecord[Team.primaryKey];

        // If we have the pubsub hook, use the Team's publish method
        // to notify all subscribers about the update.
        if (req._sails.hooks.pubsub) {
          if (req.isSocket) { Team.subscribe(req, _.pluck(records, Team.primaryKey)); }
          // The _.cloneDeep calls ensure that only plain dictionaries are broadcast.
          // TODO -- why is that important?
          Team._publishUpdate(pk, _.cloneDeep(queryOptions.valuesToSet), !req.options.mirror && req, {
            previous: _.cloneDeep(matchingRecord)
          });
        }

        // Do a final query to populate the associations of the record.
        //
        // (Note: again, this extra query could be eliminated, but it is
        //  included by default to provide a better interface for integrating
        //  front-end developers.)
        var Q = Team.findOne(_.cloneDeep(criteria), _.cloneDeep(queryOptions.populates));

        Q.exec(async function foundAgain(err, populatedRecord) {
          if (err) { return res.serverError(err); }
          if (!populatedRecord) { return res.serverError('Could not find record after updating!'); }
          const member = await TeamMember.find({team: populatedRecord.id}).populate('team').populate('member');
          populatedRecord.members = member
          res.ok(populatedRecord);
        }); // </foundAgain>
      });// </updated>
    }); // </found>
  }


};

