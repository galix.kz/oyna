/**
 * FieldController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const geolib = require('geolib');
module.exports = {
  findNearestField: async function(req,res){
    if(!req.body.long || !req.body.lat) return res.status(400).json({"message": "Please check long and lat fields"});


    const fields = await Field.find();
    // console.log(fields);
    const target = { longitude: req.body.long, latitude: req.body.lat};

    closest = geolib.orderByDistance(target, fields);
    // console.log(closest)
    for (var i=0; i<closest.length;i++){
      closest[i].field = await Field.find({"id": closest[i].key})
    }
    return res.json(closest)
  },
  uploadAvatar: function (req, res) {
    const field_id = req.body.field_id;
    if(!field_id) return res.status(400).json({"message":"Please make sure that you entered team_id"})
    console.log(req.body)
    req.file('avatar').upload({
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 10000000
    },async function whenDone(err, uploadedFiles) {
      if (err) {
        return res.serverError(err);
      }

      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0){
        return res.badRequest('No file was uploaded');
      }
      let path = require('path');
      // Get the base URL for our deployed application from our custom config
      // (e.g. this might be "http://foobar.example.com:1339" or "https://example.com")
      const baseUrl = sails.config.custom.baseUrl;
      console.log();
      const field = await Field.findOne({"id": field_id});
      let avatarUrl = field.avatarUrl;
      let fd = path.basename(uploadedFiles[0].fd);
      console.log(fd)
      if (field.avatarUrl==null) avatarUrl = [];

      avatarUrl.push(require('util').format("%s/field/avatar/%s", baseUrl, fd))
      console.log(avatarUrl)
      // Save the "fd" and the url where the avatar for a team can be accessed
      Field.update(field_id, {

        // Generate a unique URL where the avatar can be downloaded.
        avatarUrl:  avatarUrl

      })
        .exec(function (err){
          if (err) return res.serverError(err);
          return res.ok();
        });
      // res.ok()
    });
  },
  downloadAvatar: function (req, res){
    let url = req.url.split('/')[3];
    console.log(url);

    var base = process.env.PWD
    console.log(url)
      const SkipperDisk = require('skipper-disk');
      const fileAdapter = SkipperDisk({});

      fileAdapter.read(base+'/.tmp/uploads/'+url)
        .on('error', function (err){
          return res.serverError(err);
        })
        .pipe(res);
  },
  update_rating: async function(req,res) {
    if(!req.body.rating || !req.body.field_id) return res.status(400).json({"message": 'Please set rating and field_id'});
    let rating = req.body.rating;
    let field_id = req.body.field_id;

    let field = await Field.findOne({'id': field_id});
    if (!field) return res.status(404).json({"message": "There is no field with this ID"})

    let rating_sum = field.rating_sum+parseInt(rating);
    let voted_amount = field.voted_amount + 1;
    let new_rating = rating_sum/voted_amount;
    console.log(new_rating)
    field = await Field.update(field_id, {rating_sum: rating_sum, voted_amount: voted_amount, rating: new_rating}).fetch()
    return res.json(field);
  }

};

