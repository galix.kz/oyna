FROM node:8.11.3-alpine
RUN mkdir /usr/app
WORKDIR /usr/app

COPY package.json /usr/app/
RUN npm install --quite
RUN npm install -g nodemon
RUN npm install cors --save
COPY . /usr/app
